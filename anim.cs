﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class anim : MonoBehaviour {
    public GameObject Drzwi;
    // Use this for initialization
    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<Collider>().CompareTag("Player"))
        {
            Drzwi.GetComponent<Animation>().Play("test123");
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.GetComponent<Collider>().CompareTag("Player"))
        {
            Drzwi.GetComponent<Animation>().Play("test234");
        }
    }
}
